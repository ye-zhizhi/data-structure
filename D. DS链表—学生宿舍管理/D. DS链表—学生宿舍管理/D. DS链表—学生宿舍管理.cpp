﻿// D. DS链表—学生宿舍管理.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <string>
using namespace std;

class ListNode {
public:
	int data;	//房间号 
	string name;	//住户名 
	ListNode* next;
	ListNode()
	{
		next = NULL;
	}
};

class LinkList
{
public:
	ListNode* head;
	int len;
	LinkList() 
	{
		head = new ListNode;
		len = 0;
	}
	~LinkList() 
	{
		ListNode* p, * q;
		p = head;
		while (p != NULL) 
		{
			q = p;
			p = p->next;
			delete q;
		}
		len = 0;
		head = NULL;
	}
	ListNode* LL_index(int i);//返回第i个链表的结点 
	ListNode push_back(int num, string na = "无");////放到链表尾 
	int LL_get(int i);//获取房间号 
	int LL_insert(int i, string na, int item);//插入结点
	int LL_del(int i);//删除结点
	void display_free();////输出可用宿舍链表信息。
	void display_used();////输出已用宿舍链表信息
};
ListNode* LinkList::LL_index(int i)//返回第i个链表的结点
{
	ListNode* p = head;
	for (int j = 1; j <= i; j++)
	{
		p = p->next;
	}
	return p;
}
ListNode LinkList::push_back(int num, string na)//放到链表尾 
{
	ListNode* p = new ListNode;//开辟新的结点
	ListNode* q = LL_index(len);//尾部的结点
	p->data = num;
	p->name = na;
	q->next = p;
	len++;
	return *p;
}
int LinkList::LL_get(int i)//获取房间号 
{
	if (i<0 || i>len)
	{
		return -1;
	}
	else
	{
		ListNode* p = LL_index(i);
		return p->data;
	}
}
int LinkList::LL_insert(int i, string na, int item)//插入结点
{
	if (i > 0 && i <= len + 1)
	{
		ListNode* q = new ListNode;
		ListNode* p = LL_index(i-1);//先得到前一个结点
		ListNode* temp;
		temp = p->next;//先存储下一个结点
		q->data = item;
		q->name = na;
		p->next = q;
		q->next = temp;//串联结点
		len++;
		return 1;
	}
	else
	{
		return 0;
	}
}
int LinkList::LL_del(int i)//删除第i个结点
{
	if (i > 0 && i <= len) 
	{
		ListNode* p = LL_index(i - 1);//得到上一个结点
		ListNode* temp = p->next;//要删除的结点
		p->next = p->next->next;//指向下下个结点
		len--;
		temp->next = NULL;
		delete temp;
	}
	else
	{
		cout << "error" << endl;
	}
	return 1;
}
void LinkList::display_free()//输出可用宿舍链表信息。
{
	ListNode* p = head->next;
	while (p)
	{
		cout << p->data;
		if (p->next)
		{
			cout << "-";
		}
		p = p->next;
	}
	cout << endl;
}
void LinkList::display_used()//输出已用宿舍链表信息
{
	ListNode* p = head->next;
	while (p)
	{
		cout << p->name << "(" << p->data << ")";
		if (p->next)
		{
			cout << "-";
		}
		p = p->next;
	}
	cout << endl;
}
int main()
{
	int n, room;
	string name, order;
	LinkList free_room;
	for (int i = 101; i <= 120; i++)//创建101~120房间号的空房间链表
	{
		free_room.push_back(i, "无");
	}
	LinkList used_room;
	cin >> n;//人数初始化
	while (n--)
	{
		cin >> name >> room;
		used_room.push_back(room, name); //把房间号和住户名传入已用房间链表 
		for (int i = 1; i <= free_room.len; i++)//在对应的空房间链表找到对应的房间号并删除
		{
			if (free_room.LL_get(i) == room)
			{
				free_room.LL_del(i);
				break;
			}
		}
	}
	cin >> n;//修改房间数
	while (n--)
	{
		cin >> order;//命令
		if (order == "assign")//分配新宿舍
		{
			cin >> name;
			room = free_room.LL_get(1);//获得第一个房间号安排给新住户
			free_room.LL_del(1);
			for (int i = 1; i <= used_room.len; i++)
			{
				if (used_room.LL_get(i) > room)//按房间号顺序插入新住户 
				{
					used_room.LL_insert(i, name, room);
					//如果以使用房间的第一个号码大于新分配的号码，则在其前面插入，从大到小
					break;
				}
			}
		}
		if (order == "return")//退宿舍
		{
			cin >> room;
			for (int i = 1; i <= used_room.len; i++)
			{
				if (used_room.LL_get(i) == room)//删除要退房的住户 
				{
					used_room.LL_del(i);
				}
			}
			free_room.push_back(room);//空房间放回空房间链表 
		}
		if (order == "display_used") 
		{
			used_room.display_used();
		}
		if (order == "display_free") 
		{
			free_room.display_free();
		}
	}
}

