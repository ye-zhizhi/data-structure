﻿// C. DS队列--组队列（不使用STL队列）.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
using namespace std;

//队列节点
class QueueNode
{
public:
	int data;
	QueueNode* next;
	QueueNode()
	{
		data = 0;
		next = NULL;
	}
};

class LinkQueue
{
public:
	QueueNode* head;
	QueueNode* tail;
	int len;

	//接口
	LinkQueue();
	~LinkQueue();

	void InitQueue(int n);//插入队列
	void QueuePush(int x);//推送元素
	bool IsEmpty();//判断是否为空
	void QueuePop();//删除
	void PrintQueue();//打印
	bool Match(int x);//匹配
	int QueueTop();//队列顶元素

};

LinkQueue::LinkQueue()
{
	head = new QueueNode();
	tail = new QueueNode();
	head = tail;
	tail->next = NULL;
}
LinkQueue::~LinkQueue()
{
	QueueNode* cur = head->next;
	while (cur)
	{
		QueueNode* next = cur->next;
		free(cur);
		cur = next;
	}
	head->next = tail->next = NULL;
}

void LinkQueue::InitQueue(int n)
{
	for (int i = 0; i < n; i++)
	{
		int tmp;
		cin >> tmp;
		this->QueuePush(tmp);
	}
}

void LinkQueue::QueuePush(int data)
{
	QueueNode* newnode = new QueueNode();
	newnode->data = data;
	newnode->next = NULL;
	tail->next = newnode;
	tail = newnode;
}

void LinkQueue::QueuePop()
{
	QueueNode* temp = head->next;
	head->next = temp->next;
	if (tail == temp)
	{
		tail = head;
	}
	free(temp);
}

int LinkQueue::QueueTop()
{
	return head->next->data;
}

bool LinkQueue::Match(int data)
{
	QueueNode* temp = head->next;
	while (temp != NULL)
	{
		if (temp->data == data)
		{
			return true;
		}
		temp = temp->next;
	}
	return false;
}

bool LinkQueue::IsEmpty()
{
	if (head == tail)
	{
		return true;
	}
	return false;
}

void LinkQueue::PrintQueue()
{
	QueueNode* tmp = head->next;
	while (tmp != NULL)
	{
		if (tmp->next == NULL)
		{
			cout << tmp->data;
			break;
		}
		else
		{
			cout << tmp->data << " ";
			tmp = tmp->next;
		}
	}
	cout << endl;
}

int main()
{
	int t = 0;
	cin >> t;
	LinkQueue* list = new LinkQueue[t];
	for (int i = 0; i < t; i++)
	{
		int n = 0;
		cin >> n;
		list[i].InitQueue(n);
	}
	string order;
	LinkQueue my;
	LinkQueue* group = new LinkQueue[t];
	//创建新入队列的组

	while (cin >> order)
	{
		if (order == "STOP")
		{
			break;
		}
		else if(order=="ENQUEUE")
		{
			int x;
			cin >> x;
			for (int i = 0; i < t; i++)
			{
				if (group[i].IsEmpty())
				{
					group[i].QueuePush(x);
					break;
				}
				else//不为空则要判断插入位置
				{
					int flag = 0;
					for (int j = 0; j < t; j++)//分组寻找
					{
						//先判断要插入的组在哪里，再判断插入元素是否与改组匹配
						if (list[j].Match(group[i].QueueTop()) && list[j].Match(x))
						{
							flag = 1;
						}
					}
					if (flag == 1)
					{
						group[i].QueuePush(x);
						break;
					}
				}
			}
		}
		else if (order == "DEQUEUE")
		{
			for (int i = 0; i < t; i++)
			{
				if (!group[i].IsEmpty())
				{
					my.QueuePush(group[i].QueueTop());
					group[i].QueuePop();
					break;
				}
			}
		}
	}
	my.PrintQueue();
}