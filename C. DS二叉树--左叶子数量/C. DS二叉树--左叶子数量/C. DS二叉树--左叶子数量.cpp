﻿// C. DS二叉树--左叶子数量.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include<iostream>
#include<string>
#include<queue>
using namespace std;

class TreeNode {
public:
	char data;
	TreeNode* left;
	TreeNode* right;
	TreeNode()
	{
		left = NULL;
		right = NULL;
	}
};

class Tree {
private:
	queue<char> q;
	int i = 0;
	string strtree;
	TreeNode* root;
	TreeNode* Createtree()
	{
		TreeNode* t;
		char ch;
		ch = strtree[i];
		i++;
		if (ch == '0')
		{
			t = NULL;
		}
		else
		{
			t = new TreeNode();
			t->data = ch;
			t->left = Createtree();//先序创建
			t->right = Createtree();
		}
		return t;
	}
	int cnt = 0;
	void Findleaf(TreeNode* f)
	{           	
		if (f)
		{
			if (f->left)
			{
				if (f->left->left == NULL && f->left->right == NULL)
				{
					cnt++;
				}
				Findleaf(f->left);
				Findleaf(f->right);
			}
		}
	}
public:
	Tree() {}
	~Tree() {}
	void PrevCreate(string str)
	{
		i = 0;
		strtree.assign(str);//string赋值，用于两个字符串之间
		root = Createtree();
	}
	void Leaf()
	{
		cnt = 0;
		Findleaf(root);
		cout << cnt << endl;
	}
};

int main()
{
	int t;
	cin >> t;
	string str;
	Tree tr;
	while (t--)
	{
		cin >> str;
		tr.PrevCreate(str);
		tr.Leaf();
	}
}
