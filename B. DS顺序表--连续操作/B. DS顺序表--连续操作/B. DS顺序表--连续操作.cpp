﻿// ConsoleApplication1.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
using namespace std;
#define ok 0
#define error -1
//顺序表类定义
class SeqList
{
private:
    int* list;//元素数组
    int maxsize;//顺序表最大长度
    int size;//顺序表实际长度
public:
    SeqList()//构造函数
    {
        maxsize = 1000;
        size = 0;
        list = new int[maxsize];
    }
    ~SeqList()  //析构函数
    {
        delete[]list;
    }
    int list_insert(int i, int item) //插入一个元素，参数是插入的数值和位置
    {
        if (i <= 0 || i > size + 1)
        {
            return error;
        }
        else
        {
            size++;
            if (list[i - 1])
            {
                for (int j = size - 1; j >= i - 1; j--)
                {
                    list[j] = list[j - 1];
                }
                list[i - 1] = item;
                return ok;
            }
        }
    }
    //插入多个数据的函数实现在第i个位置，连续插入来自数组item的n个数据，即从位置i开始插入多个数据。
    void multiinsert(int i, int n, int item[])
    {
        for (int j = 0; j < n; j++)
        {
            list_insert(i + j, item[j]);
        }
    }
    int list_del(int i) //删除一个元素，参数是删除的位置
    {
        if (i > size)
        {
            return error;
        }
        else
        {
            size--;
            for (int j = i - 1; j < size; j++)
            {
                list[j] = list[j + 1];
            }
            return ok;
        }
    }
    //删除多个数据的函数，实现从第i个位置开始，连续删除n个数据，即从位置i开始删除多个数据。
    void multidel(int i, int n)
    {
        for (int j = 0; j < n; j++)
        {
            list_del(i);
        }
    }
    void list_display()
    {
        cout << size << " ";
        for (int i = 0; i < size; i++)
        {
            cout << list[i] << " ";
        }
        cout << endl;
    }
};

int main()
{
    int n;
    cin >> n;
    int num;
    SeqList q;
    int arr[1000];
    for (int i = 0; i < n; i++)
    {
        cin >> num;
        q.list_insert(i + 1, num);
    }
    q.list_display();

    int insert, k;
    cin >> insert >> k;
    for (int i = 0; i < k; i++)
    {
        cin >> arr[i];
    }
    q.multiinsert(insert, k, arr);
    q.list_display();

    int del;
    cin >> del >> k;
    q.multidel(del, k);
    q.list_display();
    return 0;
}