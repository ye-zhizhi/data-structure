﻿#include<iostream> 
using namespace std;
class TreeNode {
public:
	char data;
	TreeNode* left;
	TreeNode* right;
	TreeNode() :left(NULL), right(NULL) {}
	~TreeNode() {}
};
class Tree {
private:
	TreeNode* Root;
	int pos;
	string strTree;
	TreeNode* CreateTree();
	void PreOrder(TreeNode* t);
	void InOrder(TreeNode* t);
	void PostOrder(TreeNode* t);
	void LevelOrder(TreeNode* root, int i);
	int Depth(TreeNode* T);
	void Mirror(TreeNode* t);
public:
	Tree() {};
	~Tree() {};
	void CreateTree(string TreeArray);
	void PreOrder();
	void InOrder();
	void PostOrder();
	void LevelOrder();
	void Mirror();
};
void Tree::CreateTree(string TreeArray) {//建立二叉树 
	pos = 0;
	strTree.assign(TreeArray);
	Root = CreateTree();
}
TreeNode* Tree::CreateTree()
{
	TreeNode* T;
	char ch;
	ch = strTree[pos++];
	if (ch == '#') T = NULL;
	else
	{
		T = new TreeNode();
		T->data = ch;
		T->left = CreateTree();
		T->right = CreateTree();
	}
	return T;
}
//求二叉树深度
int Tree::Depth(TreeNode* T)
{
	int m, n;
	if (T == NULL)
		return 0; //如果是空树，深度为0，递归结束
	else
	{
		m = Depth(T->left); //递归计算左子树的深度记为m
		n = Depth(T->right); //递归计算右子树的深度记为n
		if (m > n)//二叉树的深度为m 与n的较大者加1
			return (m + 1);
		else
			return (n + 1);
	}
}
//层次遍历序列 
void Tree::LevelOrder() 
{
	if (Root == NULL) 
		cout << "NULL";
	int deep = Depth(Root);
	for (int i = 1; i <= deep; i++) 
	{
		LevelOrder(Root, i);
	}
}
//层次遍历具体函数 
void Tree::LevelOrder(TreeNode* root, int i) 
{
	if (root == NULL || i == 0) 
		return;
	if (i == 1) 
	{
		cout << root->data << " ";
		return;
	}
	LevelOrder(root->left, i - 1);
	LevelOrder(root->right, i - 1);
}
//镜面反转内部调用函数 
void Tree::Mirror() 
{
	Mirror(Root);
}
//镜面反转外部调用函数
void Tree::Mirror(TreeNode* t)
{
	if (t == NULL)
	{
		return;
	}
	swap(t->left, t->right);//交换左右子树 
	Mirror(t->left);
	Mirror(t->right);
}
//先序 
void Tree::PreOrder()
{
	if (Root == NULL) cout << "NULL";
	PreOrder(Root);
}
void Tree::PreOrder(TreeNode* t) 
{
	if (t)
	{
		cout << t->data << " ";
		PreOrder(t->left);
		PreOrder(t->right);
	}
}
//中序 
void Tree::InOrder()
{
	if (Root == NULL) 
		cout << "NULL";
	InOrder(Root);
}
void Tree::InOrder(TreeNode* t) 
{
	if (t) 
	{
		InOrder(t->left);
		cout << t->data << " ";
		InOrder(t->right);
	}
}
//后序 
void Tree::PostOrder()
{
	if (Root == NULL) 
		cout << "NULL";
	PostOrder(Root);
}
void Tree::PostOrder(TreeNode* t) 
{
	if (t) 
	{
		PostOrder(t->left);
		PostOrder(t->right);
		cout << t->data << " ";
	}
}
int main() 
{
	int t, i;
	cin >> t;
	for (i = 0; i < t; i++) 
	{
		string s;
		cin >> s;
		Tree* T = new Tree();
		T->CreateTree(s);
		T->Mirror();
		T->PreOrder();
		cout << endl;
		T->InOrder();
		cout << endl;
		T->PostOrder();
		cout << endl;
		T->LevelOrder();
		cout << endl;
	}
	return 0;
}
