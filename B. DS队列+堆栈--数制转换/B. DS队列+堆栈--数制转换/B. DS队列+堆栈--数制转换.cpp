﻿// B. DS队列+堆栈--数制转换.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//
#include <iostream>
#include <stack>
#include <queue>
#include <cmath>
using namespace std;

int main()
{
	int t = 0;
	cin >> t;
	while (t--)
	{
		stack<int>zheng;
		queue<int>xiao;
		double number = 0;
		cin >> number;
		int type = 0;
		cin >> type;
		int zhengshu = 0;
		double xiaoshu = 0;

		zhengshu = (int)number;
		xiaoshu = number - zhengshu;

		int cnt1 = 0;
		int cnt2 = 0;
		//开始转换
		//一 整数
		while (zhengshu > 0)
		{
			int temp = zhengshu % type;
			zheng.push(temp);
			zhengshu /= type;
			cnt1++;
		}

		//二 小数
		while (1)
		{
			int temp = (int)(xiaoshu * type);
			xiao.push(temp);
			xiaoshu = (xiaoshu * type) - temp;
			cnt2++;
			if (cnt2 >= 3)
			{
				break;
			}
		}


		for (int i = 0; i < cnt1; i++)
		{
			int temp = zheng.top();
			if (type > 10&&temp>=10)
			{
				
				int temp1 = temp % 10;
				cout << (char)('A' + temp1);
			}
			else
			{
				cout << temp;
			}
			zheng.pop();
		}

		cout << ".";
		for (int i = 0; i < cnt2; i++)
		{
			int temp= xiao.front();
			if (type > 10 && temp >= 10)
			{
				int temp1 = temp % 10;
				cout << (char)('A' + temp1);
			}
			else
			{
				cout << temp;
			}
			xiao.pop();
		}
		cout << endl;
	}
	return 0;
}
