﻿// E. 三串合一（指针与字符数组）.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
using namespace std;

int main()
{
    int t = 0;
    cin >> t;
    while (t--)
    {
        char a[10] = { 0 };
        char b[10] = { 0 };
        char c[10] = { 0 };
        for (int i = 0; i < 10; i++)
        {
            cin >> a[i];
        }
        for (int i = 0; i < 10; i++)
        {
            cin >> b[i];
        }
        for (int i = 0; i < 10; i++)
        {
            cin >> c[i];
        }
        char* p1 = a;
        char* p2 = b;
        char* p3 = c;

        int a1, a2, b1, b2, c1, c2;
        cin >> a1 >> a2 >> b1 >> b2 >> c1 >> c2;

        p1 = p1 + a1 - 1;
        p2 = p2 + b1 - 1;
        p3 = p3 + c1 - 1;

        int n = 0;
        int s1 = a2 - a1 + 1;
        int s2 = b2 - b1 + 1;
        int s3 = c2 - c1 + 1;

        n = s1 + s2 + s3;
        char* str = new char[n+1];
        char* p = str;
        //数组a
            while (s1)
            {
                *p = *p1;
                p++;
                p1++;
                s1--;
            }
            while (s2)
            {
                *p = *p2;
                p++;
                p2++;
                s2--;
            }
            while (s3)
            {
                *p = *p3;
                p++;
                p3++;
                s3--;
            }
        cout << str << endl;
        delete[]str;
    }

}

