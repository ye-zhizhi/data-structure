﻿// D. DS树--二叉树之最大路径.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include<iostream>
#include<string>
using namespace std;

class TreeNode {
public:
	char data;
	int weight;//权值
	int height;//高度
	TreeNode* left;
	TreeNode* right;
	TreeNode()
	{

		left = NULL;
		right = NULL;
	}
};

class Tree {
private:
	int i = 0;
	string strtree;
	TreeNode* root;
	int pos, p;
	int APL = 0;//带权值
	TreeNode* Createtree(int* w, int h)
	{
		TreeNode* t;
		char ch;
		ch = strtree[i];
		i++;
		if (ch == '0')
		{
			t = NULL;
		}
		else
		{
			t = new TreeNode();
			t->data = ch;
			t->height = h + 1;//高度初始为-1，
			if (t->data >= 'A' && t->data <= 'Z')
			{
				t->weight = w[p];//记录权值的数组
				p++;
			}
			t->left = Createtree(w, t->height);
			t->right = Createtree(w, t->height);
		}
		return t;
	}
	int m = 0;
	void preorder(TreeNode* t,int*apl,int rootapl)//先序遍历
	{
		
		if (t)
		{
			APL += t->weight ;//这个结点的路径和, 权值
			preorder(t->left, apl,rootapl);
			preorder(t->right, apl, rootapl);
			if (t->left == NULL && t->right == NULL)
			{
				apl[m++] = APL;
				APL = rootapl;
			}

		}
	}

public:
	Tree() {}
	~Tree() {}
	void Create(string str, int* w)
	{
		i = 0;
		pos = 0;
		p = 0;
		strtree.assign(str);//string赋值，用于两个字符串之间
		root = Createtree(w, 0);
	}
	void preorder()
	{
		int* apl = new int[m];
		root->height = 0;//根结点的高度是0
		preorder(root,apl,root->weight);
		
		int maxapl = apl[0];
		for (int i = 0; i < m; i++)
		{
			if (maxapl <= apl[i])
			{
				maxapl = apl[i];
			}
		}
		cout << maxapl << endl;
	}
};

int main()
{
	int t, n;
	cin >> t;

	while (t--)
	{
		string str;
		int w[100];
		Tree tr;
		cin >> str >> n;

		for (int i = 0; i < n; i++)
		{
			cin >> w[i];//n个叶子的权值
		}
		tr.Create(str, w);
		tr.preorder();//cout<<APL
	}
}
