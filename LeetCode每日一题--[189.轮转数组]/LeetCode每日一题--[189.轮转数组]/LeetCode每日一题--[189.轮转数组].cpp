﻿// LeetCode每日一题--[189.轮转数组].cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//
//给定一个整数数组 nums，将数组中的元素向右轮转 k 个位置，其中 k 是非负数。
//
//示例 1:
//
//输入: nums = [1, 2, 3, 4, 5, 6, 7], k = 3
//输出 : [5, 6, 7, 1, 2, 3, 4]
//解释 :
//    向右轮转 1 步 : [7, 1, 2, 3, 4, 5, 6]
//    向右轮转 2 步 : [6, 7, 1, 2, 3, 4, 5]
//    向右轮转 3 步 : [5, 6, 7, 1, 2, 3, 4]
//    示例 2 :
//
//    输入：nums = [-1, -100, 3, 99], k = 2
//    输出：[3, 99, -1, -100]
//    解释 :
//    向右轮转 1 步 : [99, -1, -100, 3]
//    向右轮转 2 步 : [3, 99, -1, -100]
//
//
//    提示：
//
//    1 <= nums.length <= 105
//    - 231 <= nums[i] <= 231 - 1
//    0 <= k <= 105
//​
//
// 
//使用额外的数组
//我们可以使用额外的数组来将每个元素放至正确的位置。用 nn 表示数组的长度，我们遍历原数组，
//将原数组下标为 ii 的元素放至新数组下标为(i + k)\bmod n(i + k)modn 的位置，最后将新数组拷贝至原数组即可。


void rotate(int* nums, int numsSize, int k) {
    int newArr[numsSize];
    for (int i = 0; i < numsSize; ++i) {
        newArr[(i + k) % numsSize] = nums[i];
    }
    for (int i = 0; i < numsSize; ++i) {
        nums[i] = newArr[i];
    }
}