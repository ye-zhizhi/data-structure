﻿// D. DS堆栈--迷宫求解.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <stack>
using namespace std;

typedef struct Postion//存储位置
{
    int x = 0;
    int y = 0;
}PT;

stack<PT>path;//定义一个全局栈
//path1是一个临时堆栈，把path的数据倒序输出到path1，使得路径按正序输出
stack<PT>path1;
bool IsPass(int** maze, int row, int col, PT pos)//判断是否能走
{
    if (pos.x >= 0 && pos.x < row && pos.y >= 0 && pos.y < col &&maze[pos.x][pos.y]==0)
    {
        return true;
    }
    else
    {
        return false;
    }
}
bool GetMazePath(int **maze,int x,int y,PT cur)
{
    path.push(cur);//入栈
    //如果走到出口
    if (cur.x == x - 1 && cur.y == y - 1)
    {
            return true;
    }

    //探测cur四个方向的位置
    //走过的位置置为2
    PT next;
    maze[cur.x][cur.y] = 2;

    //向右    列y加一
    next = cur;
    next.y += 1;
    if (IsPass(maze, x, y, next))//递归
    {
        if (GetMazePath(maze, x, y, next))
        {
            return true;
        }
    }

    //向下    行x加一
    next = cur;
    next.x += 1;
    if (IsPass(maze, x, y, next))//递归
    {
        if (GetMazePath(maze, x, y, next))
        {
            return true;
        }
    }

    //向左    列y减一
    next = cur;
    next.y -= 1;
    if (IsPass(maze, x, y, next))//递归
    {
        if (GetMazePath(maze, x, y, next))
        {
            return true;
        }
    }

    //向上    行x减一
    next = cur;
    next.x -= 1;
    if (IsPass(maze, x, y, next))//递归
    {
        if (GetMazePath(maze, x, y, next))
        {
            return true;
        }
    }

    //如果都不能走
    path.pop();//出栈
    return false;
}

int main()
{
    int t = 0;
    cin >> t;
    int x = 0;//行
    int y = 0;//列
    while (t--)
    {
        cin >> x;
        y = x;
        //动态开辟二维数组
        int** maze = new int* [x];
        for (int i = 0; i < x; i++)
        {
            maze[i] = new int[y];
        }
        //二维数组输入
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                cin >> maze[i][j];
            }
        }
        PT entry = { 0,0 };//入口
        if (maze[0][0] != 1)
        {
            if (GetMazePath(maze, x, y, entry))
            {
                {
                    PT temp;
                    while (!path.empty()) //找到路径
                    {
                        temp = path.top();
                        path.pop();
                        path1.push(temp);
                    }
                    int i = 0;
                    while (!path1.empty())
                    {
                        PT cpos = path1.top();
                        if ((++i) % 4 == 0)
                        {
                            cout << '[' << cpos.x << ',' << cpos.y << ']' << "--" << endl;
                        }
                        else
                        {
                            cout << '[' << cpos.x << ',' << cpos.y << ']' << "--";

                        }
                        path1.pop();
                    }
                    cout << "END" << endl;
                }
            }
            else
            {
                cout << "no path" << endl;
            }
        }
        else
        {
            cout << "no path" << endl;
        }
        for (int i = 0; i < x; i++)
        {
            delete[]maze[i];//表示释放掉之前每行所设的一维数组，也就是列
        }
        delete[]maze;   //最后释放掉行
    }
}

