﻿// B. DS二叉树——二叉树之父子结点.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include<iostream>
#include<string>
#include<queue>
using namespace std;

class TreeNode {
public:
	char data;
	TreeNode* left;
	TreeNode* right;
	TreeNode()
	{
		left = NULL;
		right = NULL;
	}
};

class Tree {
private:
	queue<char> q;
	int i = 0;
	string strtree;
	TreeNode* root;
	TreeNode* Createtree() 
	{
		TreeNode* t;
		char ch;
		ch = strtree[i];
		i++;
		if (ch == '0')
		{
			t = NULL;
		}
		else 
		{
			t = new TreeNode();
			t->data = ch;
			t->left = Createtree();
			t->right = Createtree();
		}
		return t;
	}
	void Findleaf(TreeNode* f, char f1) 
	{           //f1是f的父结点的data
		if (f) 
		{
			if (!f->left && !f->right) 
			{//若为叶子结点
				cout << f->data << " ";//输出叶子结点
				q.push(f1);//保存父结点
			}
			Findleaf(f->left, f->data);
			Findleaf(f->right, f->data);
		}

	}
public:
	Tree() {}
	~Tree() {}
	void Create(string str) 
	{
		i = 0;
		strtree.assign(str);//string赋值，用于两个字符串之间
		root = Createtree();
	}
	void Leaf() 
	{
		Findleaf(root, root->data);
		cout << endl;
		while (!q.empty()) 
		{
			char p = q.front();
			cout << p << " ";
			q.pop();
		}
		cout << endl;
	}
};

int main()
{
	int t;
	cin >> t;
	string str;
	Tree tr;
	while (t--) 
	{
		cin >> str;
		tr.Create(str);
		tr.Leaf();
	}
}


