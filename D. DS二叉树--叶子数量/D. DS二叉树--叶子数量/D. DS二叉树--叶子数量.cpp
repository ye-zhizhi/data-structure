﻿// D. DS二叉树--叶子数量.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include<iostream>
#include<string>
#include<queue>
using namespace std;

class TreeNode {
public:
	char data;
	TreeNode* left;
	TreeNode* right;
	TreeNode()
	{
		left = NULL;
		right = NULL;
	}
};

class Tree {
private:
	queue<char> q;
	int i = 0;
	string strtree;
	TreeNode* root;
	TreeNode* Createtree()
	{
		TreeNode* t;
		char ch;
		ch = strtree[i];
		i++;
		if (ch == '#')
		{
			t = NULL;
		}
		else
		{
			t = new TreeNode();
			t->data = ch;
			t->left = Createtree();//先序创建
			t->right = Createtree();
		}
		return t;
	}
	int cnt = 0;
	//void Findleaf(TreeNode* f)
	//{
	//	if (f)
	//	{
	//		if (f->left == NULL && f->right == NULL)
	//		{
	//			cnt++;
	//		}
	//		Findleaf(f->left);
	//		Findleaf(f->right);
	//	}
	//}
public:
	Tree() {}
	~Tree() {}
	void Create(string str)
	{
		i = 0;
		strtree.assign(str);//string赋值，用于两个字符串之间
		root = Createtree();
		PrevPrint(root);
		cout << endl;
		MidPrint(root);
		cout << endl;
		FinPrint(root);
	}

	void PrevPrint(TreeNode* root)
	{
		if (root == NULL)
		{
			return;
		}

		cout << root->data;
		PrevPrint(root->left);
		PrevPrint(root->right);
	}

	void MidPrint(TreeNode* root)
	{
		if (root == NULL)
		{
			return;
		}

		MidPrint(root->left);
		cout << root->data;
		MidPrint(root->right);
	}

	void FinPrint(TreeNode* root)
	{
		if (root == NULL)
		{
			return;
		}

		FinPrint(root->left);
		FinPrint(root->right);
		cout << root->data;
	}
};

int main()
{
	int t;
	cin >> t;
	string str;
	Tree tr;
	while (t--)
	{
		cin >> str;
		tr.Create(str);
	}
}
