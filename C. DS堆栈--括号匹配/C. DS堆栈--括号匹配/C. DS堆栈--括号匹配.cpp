﻿// ConsoleApplication4.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <stack>
#include <string>
using namespace std;
int test(string str)
{
	int len = str.length();
	stack<char>s1, s2, s3;
	for (int i = 0; i < len; i++)
	{
		if (str[i] == '(')
		{
			s1.push(str[i]);
		}
		else if (str[i] == ')')
		{
			if (!s1.empty())
			{
				s1.pop();
			}
			else
			{
				s1.push(str[i]);
			}
		}
	}
	if (!s1.empty())
	{
		return 1;
	}
	for (int i = 0; i < len; i++)
	{
		if (str[i] == '{')
		{
			s2.push(str[i]);
		}
		else if (str[i] == '}')
		{
			if (!s2.empty())
			{
				s2.pop();
			}
			else
			{
				s2.push(str[i]);
			}
		}
	}
	if (!s2.empty())
	{
		return 1;
	}
	for (int i = 0; i < len; i++)
	{
		if (str[i] == '[')
		{
			s3.push(str[i]);
		}
		else if (str[i] == ']')
		{
			if (!s3.empty())
			{
				s3.pop();
			}
			else
			{
				s3.push(str[i]);
			}
		}
	}
	if (!s3.empty())
	{
		return 1;
	}
	return 0;
}

int main()
{
	int t = 0;
	cin >> t;
	while (t--)
	{
		string str;
		int len;
		cin >> str; //把输入的字符串保存在变量str;
		if (test(str))
		{
			cout << "error";
		}
		else
		{
			cout << "ok";
		}
		cout << endl;
	}

}
