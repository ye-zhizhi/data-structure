﻿// E. DS线性表—多项式相加.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
using namespace std;
#define ok 0
#define error -1
class ListNode 
{
public:
    int xishu;//系数
    int zhishu;//指数
    ListNode* next;
    ListNode() {
        next = NULL;
    }
};
class LinkList
{
public:
    ListNode* head;
    int len;
    LinkList() 
    {
        head = new ListNode();
        len = 0;
    }
    ~LinkList()
    {
        ListNode* p, * q;
        p = head;
        while (p != NULL)
        {
            q = p;
            p = p->next;
            delete q;
        }
        len = 0;
        head = NULL;
    }
    void LL_create(int n);//创建多项式
    void LL_add(LinkList& a);
    void LL_display();//输出
};
void  LinkList::LL_create(int n)
{
        ListNode* p, * q;
        p = head;
        int z, x;
        for (int i = 1; i <= n; i++) 
        {
            q = new ListNode;
            cin >> x >> z;
            q->xishu = x;
            q->zhishu = z;
            p->next = q;
            p = p->next;
            len++;
        }
}
void LinkList::LL_add(LinkList& a)
{
    ListNode* p = head->next;
    ListNode* q = a.head->next;
    LinkList my;
    ListNode* s = my.head;
    while (p && q)
    {
        s->next = new ListNode;
        if (p->zhishu == q->zhishu)//指数相同时可以相加
        {
            if ((p->xishu + q->xishu) != 0)
            {
                s->next->xishu = p->xishu + q->xishu;
                s->next->zhishu = p->zhishu;
                s = s->next;
                q = q->next;
                p = p->next;
            }
            else//等于零直接跳过,不显示
            {
                q = q->next;
                p = p->next;
            }
        }
        else if (p->zhishu > q->zhishu)//指数大于，则取较小的一位
        {
            s->next->zhishu = q->zhishu;
            s->next->xishu = q->xishu;
            s = s->next;
            q = q->next;
        }
        else
        {
            s->next->xishu = p->xishu;
            s->next->zhishu = p->zhishu;
            s = s->next;
            p = p->next;
        }
    }
        while (p)//当q加完，只剩p
        {
            s->next = new ListNode;
            s->next->zhishu = p->zhishu;
            s->next->xishu = p->xishu;
            p = p->next;
            s = s->next;
        }
        while (q)
        {
            s->next = new ListNode;
            s->next->zhishu = q->zhishu;
            s->next->xishu = q->xishu;
            q = q->next;
            s = s->next;
        }
        s->next = NULL;
        my.LL_display();
    
}
void LinkList::LL_display()
{
    ListNode* p;
    p = head->next;
    while (p)
    {
        if (p->xishu < 0)
        {
            cout << "(" << p->xishu << ")";
            if (p->zhishu)
            {
                if (p->zhishu > 0)
                {
                    cout << "x^" << p->zhishu;
                }
                else
                {
                    cout << "x^" << "(" << p->zhishu << ")";
                }
            }
        }
        else
        {
            cout << p->xishu;
            if (p->zhishu > 0)
            {
                cout << "x^" << p->zhishu;
            }
            else if(p->zhishu < 0)
            {
                cout << "x^" << "(" << p->zhishu << ")";
            }
        }
        p = p->next;
        if (p)
        {
            cout << " + ";
        }
    }
    cout << endl;
}

int main()
{
    int n, t, zhishu, xishu;
    cin >> t;
    LinkList a;
    LinkList b;
    while (t--)
    {
        cin >> n;
        a.LL_create(n);
        a.LL_display();
        cin >> n;
        b.LL_create(n);
        b.LL_display();
        a.LL_add(b);
    }
    
}


