﻿// C. DS二叉排序树_树中第k小的元素.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include<iostream>
#include<stack>
#include<queue>
using namespace std;
class BiTreeNode
{
public:
	int val;
	BiTreeNode* lchild, * rchild;
	BiTreeNode()
	{
		lchild = NULL;
		rchild = NULL;
	}
};
class BiTree
{
public:
	BiTreeNode* root;
	int flag;
	int judge;
	BiTree()
	{
		judge = 1;
		flag = 0;
		root = NULL;
	}
	void CreateTree(int* arr, int n)
	{
		//定义一个队列，每入数据，每出一个，就入其左右节点
		queue<BiTreeNode*>myqueue;

		if (n == 0)
		{
			return;
		}
		root = new BiTreeNode();
		root->val = arr[0];
		myqueue.push(root);
		int i = 1;
		while (!myqueue.empty() && i < n)
		{
			BiTreeNode* Node = myqueue.front();//记录根
			myqueue.pop();//出一个，入左右
			if (arr[i] != -1)//入左
			{
				Node->lchild = new BiTreeNode();
				Node->lchild->val = arr[i];
				myqueue.push(Node->lchild);
			}
			i++;
			if (i == n)
			{
				break;
			}
			if (arr[i] != -1)
			{
				Node->rchild = new BiTreeNode();
				Node->rchild->val = arr[i];
				myqueue.push(Node->rchild);
			}
			i++;

		}
	}
	int kthSmallest(BiTreeNode* root, int k) 
	{
		//用栈来模拟中序遍历
		stack<BiTreeNode*> s;
		BiTreeNode* cur = root;
		int count = 0;
		while (cur != NULL || !s.empty()) 
		{
			if (cur != NULL) 
			{
				s.push(cur);
				cur = cur->lchild;
			}
			else 
			{
				cur = s.top();
				s.pop();
				count++;
				if (count == k) {
					return cur->val;
				}
				cur = cur->rchild;
			}
		}

		return 0;
	}

	void Search(int k)
	{
		int min = kthSmallest(root, k);
		cout << min << endl;
	}
};

int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		int n;
		cin >> n;
		BiTree T;
		int* arr = new int[n];
		for (int i = 0; i < n; i++)
		{
			cin >> arr[i];
		}
		T.CreateTree(arr, n);

		int k = 0;
		cin >> k;
		T.Search(k);
	}
}