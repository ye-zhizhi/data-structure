﻿// D. DS查找——折半查找求平方根.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include<iomanip>
using namespace std;

int main()
{
	int n = 0;
	cin >> n;
	while (n--)
	{
		double y;
		cin >> y;

		double low = 0;
		double high = y;
		double x = 0;
		int flag = 0;

		while (low <= high)
		{
			x = 1.000 * (low + high) / 2;
			if (x* x - y > -0.0001 && x * x - y < 0.0001)
			{
				printf("%.3lf\n", x);
				break;
			}
			else if(x * x > y)
			{
				high = x;
			}
			else
			{
				low = x;
			}
		}
	}
	return 0;
}


