﻿// C. DS队列_动物收容所.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <queue>
using namespace std;
struct Animal
{
	int number;
	string type;
	Animal() {}
	Animal(int n, string o) :number(n), type(o) {}
};
class AnimalShelf
{
public:
	queue<Animal>dogs;
	queue<Animal>cats;
	void enqueue(int number, string type);
	void dequeueAny();
	int dequeueDog();
	int dequeueCat();
};

void AnimalShelf::enqueue(int number, string type)
{
	if (type == "dog")
	{
		dogs.push(Animal(number, type));
	}
	else
	{
		cats.push(Animal(number, type));
	}
}
void AnimalShelf::dequeueAny()
{
	while (!dogs.empty() && !cats.empty())
	{
		if (dogs.front().number < cats.front().number)
		{
			cout << "[" << dogs.front().number << " " << "dog" << "]" << " ";
			dogs.pop();
			return;
		}
		else
		{
			cout << "[" << cats.front().number << " " << "cat" << "]" << " ";
			cats.pop();
			return;
		}
	}
	if (!dogs.empty())
	{
		cout << "[" << dogs.front().number << " " << "dog" << "]" << " ";
		dogs.pop();
		return;
	}
	if (!cats.empty())
	{
		cout << "[" << cats.front().number << " " << "cat" << "]" << " ";
		cats.pop();
		return;
	}
	cout << "[" << -1 << " " << "none" << "]" << " ";
	return;

}
int AnimalShelf::dequeueDog()
{
	if (!dogs.empty())
	{
		int tmp = dogs.front().number;
		dogs.pop();
		return tmp;;
	}
	return -1;
}
int AnimalShelf::dequeueCat()
{
	if (!cats.empty())
	{
		int tmp = cats.front().number;
		cats.pop();
		return tmp;
	}
	return -1;
}



int main()
{
	int number = 0;
	string order;
	string an;
	int t = 0;
	cin >> t;
	while (t--)
	{
		int n = 0;
		cin >> n;
		AnimalShelf a1;
		for (int i = 0; i < n; i++)
		{
			cin >> order;
			if (order == "enqueue")
			{
				cin >> number >> an;
				a1.enqueue(number, an);
			}
			if (order == "dequeueAny")
			{
				a1.dequeueAny();
			}
			if (order == "dequeueDog")
			{
				int tmp = a1.dequeueDog();
				if (tmp == -1)
				{
					cout << "[" << -1 << " " << "none" << "]" << " ";
				}
				else
				{
					cout << "[" << tmp << " " << "dog" << "]" << " ";
				}
			}
			if (order == "dequeueCat")
			{
				int tmp = a1.dequeueCat();
				if (tmp == -1)
				{
					cout << "[" << -1 << " " << "none" << "]" << " ";
				}
				else
				{
					cout << "[" << tmp << " " << "cat" << "]" << " ";
				}
			}
		}
		cout << endl;
	}
}