﻿// 【10分】F. 判断矩形是否重叠(结构).cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
using namespace std;

struct SPoint
{
    int x, y;
};

struct SRect
{
    SPoint p1, p2;
};

//求较大值
int Max(int a, int b)
{
    return a > b ? a : b;
}
int Min(int a, int b)
{
    return a > b ? b : a;
}
bool check(const SRect& a, const SRect& b)
{
    if (Max(a.p1.x, a.p2.x) < Min(b.p1.x, a.p2.x) || Min(a.p1.x, a.p2.x) > Max(b.p1.x, a.p2.x)
        || Max(a.p1.y, a.p2.y) < Min(b.p1.y, b.p2.y) || Min(a.p1.y, a.p2.y) > Max(b.p1.y, b.p2.y))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int main()
{
    int t;
    cin >> t;
    while (t--)
    {
        SRect r1, r2;
        cin >> r1.p1.x >> r1.p1.y >> r1.p2.x >> r1.p2.y;
        cin >> r2.p1.x >> r2.p1.y >> r2.p2.x >> r2.p2.y;
        if (check(r1, r2))
        {
            cout << "not overlapped" << endl;
        }
        else
        {
            cout << "overlapped" << endl;
        }
    }
    return 0;
}
