﻿// B. DS二叉树—二叉树结点的最大距离.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
using namespace std;

class TreeNode {
public:
	char data;
	TreeNode* left;
	TreeNode* right;
	TreeNode()
	{
		left = NULL;
		right = NULL;
	}
};

class Tree {
private:
	int pos;
	string strtree;
	TreeNode* root;
	TreeNode* Createtree();//建树私有函数
	int maxHeight;
	int maxdis;//最远距离
	void preOrder(TreeNode* T, int height);
	void mycnt(TreeNode* T);
	char leftc, rightc, tempc;
public:
	Tree() {}
	~Tree() {}
	void Create(string str);	//建树公有接口，参数是特定的先序遍历字符串
	void mycnt();
};
TreeNode* Tree::Createtree()
{
	TreeNode* t;
	char ch;
	ch = strtree[pos];
	pos++;
	if (ch == '#')
	{
		t = NULL;
	}
	else
	{
		t = new TreeNode();
		t->data = ch;
		t->left = Createtree();//先序创建
		t->right = Createtree();
	}
	return t;
}

void Tree::Create(string str)//建树公有接口，参数是特定的先序遍历字符串
{
	pos = 0;
	maxHeight = 0;
	maxdis = 0;
	strtree.assign(str);//string赋值，用于两个字符串之间
	root = Createtree();
}


void Tree::preOrder(TreeNode* T, int height)
{
	if (T->left == NULL && T->right == NULL)
	{
		if (height > maxHeight)
		{
			maxHeight = height;//记录深度，不断更新
			tempc = T->data;//记录最深的叶子
		}
	}
	if (T->left != NULL)
	{
		preOrder(T->left, height + 1);
	}	
	if (T->right != NULL)
	{
		preOrder(T->right, height + 1);
	}
		
}

void Tree::mycnt(TreeNode* T)
{
	if (T == NULL) 
		return;

	int distance = 0;
	char a = 'A', b = 'A';

	if (T->left)//每次都把该子节点当作根节点
	{
		preOrder(T->left, 1);
		a = tempc;
	}
	distance += maxHeight;
	maxHeight = 0;
	if (T->right)
	{
		preOrder(T->right, 1);
		b = tempc;
	}
	distance += maxHeight;
	maxHeight = 0;

	if (distance > maxdis)
	{
		maxdis = distance;
		leftc = a;
		rightc = b;
	}
	mycnt(T->left);
	mycnt(T->right);
}


void Tree::mycnt()
{
	mycnt(root);
	if (maxdis)
	{
		cout << maxdis << ":" << leftc << " " << rightc << endl;
	}	
	else
	{
		cout << maxdis << ":" << endl;
	}
	pos = 0;
}

int main()
{
	int t;
	cin >> t;
	string str;
	Tree tr;
	while (t--)
	{
		cin >> str;
		tr.Create(str);
		tr.mycnt();
	}
}
