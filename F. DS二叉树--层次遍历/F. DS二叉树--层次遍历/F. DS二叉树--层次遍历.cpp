﻿// F. DS二叉树--层次遍历.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include<iostream>
#include<string>
#include<queue>
using namespace std;

class TreeNode {
public:
	char data;
	TreeNode* left;
	TreeNode* right;
	TreeNode()
	{
		left = NULL;
		right = NULL;
	}
};

class Tree {
private:
	queue<char> q;
	int i = 0;
	string strtree;
	TreeNode* root = NULL;
	TreeNode* Createtree()
	{
		TreeNode* t;
		char ch;
		ch = strtree[i];
		i++;
		if (ch == '0')
		{
			t = NULL;
		}
		else
		{
			t = new TreeNode();
			t->data = ch;
			t->left = Createtree();//先序创建
			t->right = Createtree();
		}
		return t;
	}
	void LevelOrder(TreeNode* root)
	{
		queue<TreeNode*>tq;
		TreeNode* p = root;
		tq.push(p);
		while (!tq.empty())
		{
			p = tq.front();
			cout << p->data;
			tq.pop();
			if (p->left != NULL)
			{
				tq.push(p->left);
			}
			if (p->right != NULL)
			{
				tq.push(p->right);
			}
		}
		cout << endl;
	}
public:
	Tree() {}
	~Tree() {}
	void Create(string str)
	{
		strtree.assign(str);//string赋值，用于两个字符串之间
		root = Createtree();
	}
	void LevelOrder()
	{
		LevelOrder(root);
	}

};

int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		string str;
		Tree mytree;
		cin >> str;
		mytree.Create(str);
		mytree.LevelOrder();
	}
}

