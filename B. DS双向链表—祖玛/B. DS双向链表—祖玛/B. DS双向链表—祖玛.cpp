﻿// B. DS双向链表—祖玛.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <string>
using namespace std;
class ListNode
{
public:
    char num;
    ListNode* next;
    ListNode* prev;
    ListNode()
    {
        next = NULL;
        prev = NULL;
    }
};
class LinkList
{
public:
    int len;
    ListNode* head;
    LinkList()
    {
        head = new ListNode;
    }
    ~LinkList()
    {
        ListNode* p, * q;
        p = head;
        while (p->next)
        {
            q = p->next;
            delete p;
            p = q;
        }
        delete p;
    }
    void create(char* str);//创造n个结点的链表
    bool insert(int pos, char num);//插入
    bool check();//检查目前的珠子是否可以消除
    void display();//打印
};
void LinkList::create(char* str)
{
    int num;
    ListNode* p = head;

    for (int i = 0; i < strlen(str); i++)
    {
        ListNode* newnode = new ListNode();//创建新结点
        newnode->next = NULL;
        newnode->prev = NULL;

        newnode->num = str[i];
        newnode->next = p->next;
        newnode->prev = p;
        p->next = newnode;
        p = p->next;
    }
    len = strlen(str);
}
bool LinkList::check()
{
    ListNode* p = head->next;
    while (p && p->next)
    {
        ListNode* q = p;
        int sum = 1;
        while(q->next && q->num == q->next->num)
        {
            sum++;
            q = q->next;
        }
        if (sum >= 3)
        {
            p->prev->next = q->next;
            return true;
        }
        else
        {
            p = q->next;
        }
    }
    return false;
}
bool LinkList::insert(int pos, char num)
{
    if (!(pos >= 0 && pos < len))
    {
        return false;
    }
    
    ListNode* p = head;
    int j = 0;
    while (p && j < pos)//找到pos前面的那个结点
    {
        p = p->next;
        j++;
    }
    ListNode* newnode = new ListNode();
    newnode->num = num;
    newnode->next = p->next;
    newnode->prev = p;
    p->next = newnode;
    //if (p->next)//如果插入为第一个位置
    //{
    //    p->next->prev = newnode;
    //}
    //else
    //{
    //    p->next = newnode;
    //}
    return true;
}
void LinkList::display()
{
    ListNode* p = head->next;
    if (!p)
    {
        cout << "-" << endl;
        return;
    }
    while (p)
    {
        cout << p->num;
        p = p->next;
    }
    cout << endl;
}
int main()
{
    LinkList list;
    char num;
    int t, pos;
    char* str = new char[100];
    cin >> str;
    list.create(str);
    cin >> t;
    while (t--)
    {
        cin >> pos >> num;
        list.insert(pos, num);
        while (1)
        {
            if (list.check())
            {
                continue;
            }
            else
            {
                break;
            }
        }
        list.display();
    }
    return 0;
}
