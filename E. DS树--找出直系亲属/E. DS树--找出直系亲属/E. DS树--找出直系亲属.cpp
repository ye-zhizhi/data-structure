﻿#include<iostream>
#include<string>
#include<cctype>

using namespace std;
const int N = 1010;
int pre[N];
//并查集算法
int unionsearch(int a, int b)   //这里假定a是孩子，b是长辈
{
	int count = 1;
	while (b != -1)
	{
		if (pre[b] == a)
			return count;
		else
		{
			b = pre[b];
			count++;
		}
	}
	return -1;
}

int main()
{
	int m, n, sum1, sum2;
	string str;
	string ge = "great-", pa = "parent", ch = "child", ga = "grand";
	while (cin >> n >> m)
	{
		if (n == m && m == 0)    //当m, n 都是0时停止输入
			break;
		memset(pre, -1, sizeof pre);   //pre数组全部元素初始化为-1
		while (n--)
		{
			cin >> str;
			for (int i = 1; i < str.size(); i++)
			{
				if (isalpha(str[i]))
				{
					pre[str[i] - 'A'] = str[0] - 'A';  //记录亲自关系
				}
			}				
		}
		while (m--)
		{
			cin >> str;
			//由于不知道辈分关系，所以需要调用两次函数
			sum1 = unionsearch(str[0] - 'A', str[1] - 'A');
			sum2 = unionsearch(str[1] - 'A', str[0] - 'A');
			if (sum1 == sum2 && sum1 == -1)
			{
				cout << "-" << endl; //当两次调用结果全是-1，说明没有亲属关系
			}
			else if (sum1 != -1)   //sum1不为-1，说明前者是孩子
			{
				if (sum1 == 1)
				{
					cout << ch << endl;
				}
				else if (sum1 == 2)
				{
					cout << ga << ch << endl;
				}
				else
				{
					for (int i = 2; i < sum1; i++)
					{
						cout << ge;
					}
					cout << ga << ch << endl;
				}
			}
			else if (sum2 != -1)   //sum2不为-1，说明后者是孩子
			{
				if (sum2 == 1)
				{
					cout << pa << endl;
				}
				else if (sum2 == 2)
				{
					cout << ga << pa << endl;
				}
				else
				{
					for (int i = 2; i < sum2; i++)
					{
						cout << ge;
					}
					cout << ga << pa << endl;
				}
			}
		}
	}
}
