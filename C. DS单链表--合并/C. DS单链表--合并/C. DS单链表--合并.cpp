﻿#include<iostream>
using namespace std;
class ListNode
{
public:
	int data;
	ListNode* next;
	ListNode()
	{
		next = NULL;
	}
};
class LinkList
{
public:
	ListNode* head;
	int len;
	LinkList()
	{
		head = new ListNode();
		len = 0;
	}
	~LinkList()
	{
		ListNode* p = head;
		ListNode* q;
		while (p != NULL)
		{
			q = p;
			p = p->next;
			delete q;
		}
		len = 0;
		head = NULL;
	}
	ListNode* LL_index(int i);//返回第i个结点的指针,如果不存在返回NULL
	int LL_insert(int i, int item);//把数值item插入第i个位置
	int LL_swap(int pa, int pb);//交换两个节点
	int LL_merge(ListNode* La, ListNode* Lb);//合并操作
	void LL_display();//打印
};
ListNode* LinkList::LL_index(int i)
{
	ListNode* p = head;
	if (i == 0)
	{
		return p;
	}
	int j = 1;
	p = p->next;
	while ((p != NULL) && (j < i))
	{
		p = p->next;
		j++;
	}
	if ((p == NULL) || (i < j))
	{
		return 0;
	}
	else
	{
		return p;
	}
}
int LinkList::LL_insert(int i, int item)
{
	if ((i <= 0) || (i > len + 1))
	{
		return 0;
	}
	ListNode* p = LL_index(i - 1);
	if (p == NULL) {
		return 0;
	}
	ListNode* s = new ListNode();
	s->data = item;
	s->next = p->next;
	p->next = s;
	len++;
	return 1;
}
int LinkList::LL_merge(ListNode* La, ListNode* Lb)
{
	ListNode* pa = La;
	ListNode* pb = Lb;
	while (pa && pb)
	{
		if (pa->data < pb->data)
		{
			LL_insert(len + 1, pa->data);
			pa = pa->next;
		}
		else
		{
			LL_insert(len + 1, pb->data);
			pb = pb->next;
		}
	}
	while (pa) 
	{
		LL_insert(len + 1, pa->data);
		pa = pa->next;
	}
	while (pb) 
	{
		LL_insert(len + 1, pb->data);
		pb = pb->next;
	}
	return 0;
}
void LinkList::LL_display()
{
	ListNode* p = head->next;
	while (p)
	{
		cout << p->data << " ";
		p = p->next;//竟然忘记了，无止境输出11
	}
	cout << endl;
}
int main()
{
	int len, item;
	cin >> len;
	LinkList mylist1;
	LinkList mylist2;
	LinkList mylist;
	for (int i = 1; i <= len; i++)
	{
		cin >> item;
		mylist1.LL_insert(i, item);
	}
	cin >> len;
	for (int i = 1; i <= len; i++)
	{
		cin >> item;
		mylist2.LL_insert(i, item);
	}
	mylist.LL_merge(mylist1.head->next, mylist2.head->next);
	mylist.LL_display();
	return 0;
}