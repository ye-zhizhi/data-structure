#include<iostream>
#include<queue>
using namespace std;
class BiTreeNode
{
public:
	int val;
	BiTreeNode* lchild, * rchild;
	BiTreeNode()
	{
		lchild = NULL;
		rchild = NULL;
	}
};
class BiTree
{
public:
	BiTreeNode* root;
	int flag;
	int judge;
	BiTree()
	{
		judge = 1;
		flag = 0;
		root = NULL;
	}
	void CreateTree(int* arr, int n)
	{
		//定义一个队列，每入数据，每出一个，就入其左右节点
		queue<BiTreeNode*>myqueue;

		if (n == 0)
		{
			return;
		}
		root = new BiTreeNode();
		root->val = arr[0];
		myqueue.push(root);
		int i = 1;
		while (!myqueue.empty()&&i<n)
		{
			BiTreeNode* Node = myqueue.front();//记录根
			myqueue.pop();//出一个，入左右
			if (arr[i] != -1)//入左
			{
				Node->lchild = new BiTreeNode();
				Node->lchild->val = arr[i];
				myqueue.push(Node->lchild);
			}
			i++;
			if (i == n)
			{
				break;
			}
			if (arr[i] != -1)
			{
				Node->rchild = new BiTreeNode();
				Node->rchild->val = arr[i];
				myqueue.push(Node->rchild);
			}
			i++;

		}
	}
	void Search(BiTreeNode* Node)
	{
		if (Node==NULL)
		{
			return;
		}
		if (Node->lchild)
		{
			if (Node->lchild->val >= Node->val)
			{
				judge = 0;
			}
		}
		if (Node->rchild)
		{
			if (Node->rchild->val <= Node->val)
			{
				judge = 0;
			}
		}
		Search(Node->lchild);
		Search(Node->rchild);
	}
	void Judge()
	{
		Search(root);
		if (judge)
		{
			cout << "true" << endl;
		}
		else
		{
			cout << "false" << endl;
		}
	}
};



int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		int n;
		cin >> n;
		BiTree T;
		int* arr = new int[n];
		for (int i = 0; i < n; i++)
		{
			cin >> arr[i];
		}
		T.CreateTree(arr, n);
		T.Judge();
	}
}