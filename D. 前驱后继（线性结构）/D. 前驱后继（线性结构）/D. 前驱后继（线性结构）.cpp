﻿// D. 前驱后继（线性结构）.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
using namespace std;
class ListNode
{
public:
    int num;
    ListNode* next;
    ListNode* prev;
    ListNode()
    {
        next = NULL;
        prev = NULL;
    }
};
class LinkList
{
public:
    int len;
    ListNode* head;
    LinkList()
    {
        head = new ListNode;
    }
    ~LinkList()
    {
        ListNode* p, * q;
        p = head;
        while (p->next)
        {
            q = p->next;
            delete p;
            p = q;
        }
        delete p;
    }
    void create(int len);//创造链表
    ListNode* check(int key);//查找
    void display(int key);//打印
};
void LinkList::create(int n)
{
    int num;
    ListNode* p = head;
    for (int i = 1; i <= n; i++)
    {
        ListNode* newnode = new ListNode();
        newnode->next = NULL;
        newnode->prev = NULL;
        cin >> num;

        newnode->num = num;
        newnode->next = p->next;
        newnode->prev = p;
        p->next = newnode;
        p = p->next;
    }
    len = n;
}
void LinkList::display(int key)
{
    ListNode* p = head->next;
    while (p)
    {
        if (p->num == key)
        {
            if (p->prev != head)
            {
                cout << p->prev->num<<" ";
            }
            if (p->next != NULL)
            {
                cout << p->next->num;
            }
            cout << endl;
        }
        p = p->next;
    }
}
int main()
{
    LinkList list;
    int n, m, key;
    cin >> n >> m;
    list.create(n);
    while (m--)
    {
        cin >> key;
        list.display(key);
    }
}