﻿// E. DS二分查找_搜索旋转排序数组.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
using namespace std;
int find(int* a, int target,int n)
{
    //找出发生旋转的点
    int min, max=0, mid;
    for (int i = 0; i < n; i++)
    {
        if (a[0] > a[i])//分成两段，顺序为从小到大，当不满足时就是旋转点
        {
            max = i;
            break;
        }
    }
    if (max == 0)//当旋转发生在0处时，就是没有发生旋转
    {
        max = n - 1;
    }
    else if (target<a[max] || target>a[max - 1])//max为最小值，max-1为最大值
    {
        return -1;
    }

    //以旋转的地方为中心，将数组分为左半边和右半边，每个半区的数组是递增的，可以使用二分法查找
    if (target >= a[0])//左半边
    {
        min = 0;

        while (min <= max)
        {
            mid = (min + max) / 2;
            if (target == a[mid])
            {
                return mid;
            }
            else if (target > a[mid])
            {
                min = mid+1;
            }
            else
            {
                max = mid-1;
            }
        }
        return -1;
    }
    else//右半边
    {
        min = max;
        max = n - 1;
        while (min <= max)
        {
            mid = (min + max) / 2;
            if (target == a[mid])
            {
                return mid;
            }
            else if (target > a[mid])
            {
                min = mid + 1;
            }
            else
            {
                max = mid - 1;
            }
        }
        return -1;
    }
}

int main()
{
    int t = 0;
    int n = 0;
    cin >> t;
    while (t--)
    {
        cin >> n;
        int* a = new int[n];
        for (int i = 0; i < n; i++)
        {
            cin >> a[i];
        }
        int target = 0;
        cin >> target;

        int index = find(a, target, n);

        cout << index << endl;
    }
}


