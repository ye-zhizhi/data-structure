﻿#include<iostream>
using namespace std;

#define ok 0
#define error -1

class SeqList {
private:
	int* list;
	int maxsize;
	int size;
public:
	SeqList() {
		maxsize = 1000;
		size = 0;
		list = new int[maxsize];
	}
	~SeqList() {
		delete[]list;
	}
	void push_back(int i) {
		list[size] = i;
		size++;
	}
	void list_display() {
		cout << size << " ";
		for (int i = 0; i < size; i++) {
			cout << list[i] << " ";
		}
		cout << endl;
	}
	SeqList(SeqList& s1, SeqList& s2) {
		maxsize = 1000;
		size = s1.size + s2.size;
		list = new int[maxsize];
		int i = 0, j = 0, k = 0;
		while (i < s1.size && j < s2.size) {
			if (s1.list[i] < s2.list[j]) {
				list[k] = s1.list[i];
				i++;
				k++;
			}
			else {
				list[k] = s2.list[j];
				j++;
				k++;
			}
		}
		while (i < s1.size) {
			list[k] = s1.list[i];
			k++;
			i++;
		}
		while (j < s2.size) {
			list[k] = s2.list[j];
			k++;
			j++;
		}
	}
};

int main() {
	SeqList s1;
	int n, a;
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> a;
		s1.push_back(a);
	}
	SeqList s2;
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> a;
		s2.push_back(a);
	}
	SeqList s(s1, s2);
	s.list_display();
	return 0;
}
