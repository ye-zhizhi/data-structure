﻿//// F. 可重叠子串 (Ver. I).cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
////
//
#include <iostream>
using namespace std;
//void GetNext(string zi_chuan, int* next, int lenzi)
//{
//    next[0] = -1;
//    next[1] = 0;
//    int i = 2;
//    int k = 0;
//    while (i < lenzi)
//    {
//        if (k == -1 || zi_chuan[i - 1] == zi_chuan[k])
//        {
//            next[i] = k + 1;
//            k++;
//            i++;
//        }
//        else
//        {
//            k = next[k];
//        }
//    }
//}
//int KMP(string zhu_chuan, string zi_chuan,int pos)
//{
//    int lenzhu = zhu_chuan.length();
//    int lenzi = zi_chuan.length();
//    if (lenzhu == 0 || lenzi == 0)
//    {
//        return -1;
//    }
//
//    //求next数组
//    int* next = new int[lenzi];
//    GetNext(zi_chuan, next, lenzi);
//
//    int i = pos;//遍历主串
//    int j = 0;//遍历子串
//    while (i < lenzhu && j < lenzi)
//    {
//        if (j == -1 || zhu_chuan[i] == zi_chuan[j])
//        {
//            i++;
//            j++;
//        }
//        else
//        {
//            j = next[j];
//        }
//    }
//    if (j >= lenzi)
//    {
//        return i - j;
//    }
//    return -1;
//}
//
//
//int main()
//{
//    string str, sub;
//    while (cin >> str)
//    {
//        int t = 0;
//        cin >> t;
//        while (t--)
//        {
//            cin >> sub;
//            int pos = 0;//位置每次都往前移动一个字符
//            int m = 0;//次数
//            int flag = 0;
//            int i = 0;
//             for(;i<str.length();i++)
//            {
//                pos = KMP(str, sub, i);
//                if (pos != -1)
//                {    
//                    i = pos;
//                    m++;
//                }
//            }
//            cout << sub << ":" << m << endl;
//        }
//    }
//}
// 
//kmp算法在数据多的时候运行时异常，超时


int main()
{
    string str, sub;
    while (cin >> str)
    {
        int t = 0;
        cin >> t;
        while (t--)
        {
            cin >> sub;
            int len = sub.length();
            int m = 0;     
            for(int i=0;i<str.length();i++)
            {
                string str1 = str.substr(i, len);//暴力算法，一个一个比对。
                if (sub == str1)
                {
                    m++;
               }
            }
            cout << sub << ":" << m << endl;
        }
    }
}