﻿#include<iostream> 
using namespace std;
class Node {
public:
	int data;
	Node* left, * right;
	Node() :data(0) {
		left = NULL;
		right = NULL;
	}
};
class BSTree 
{
private:
	Node* root;
public:
	BSTree() 
	{
		root = new Node();
	}
	Node* getroot()
	{
		return root;
	}

	//二叉排序树的性质：左子树小于根，右子树大于根
	Node* insert(int x, Node* root) //插入     
	{
		if (root->data == 0) 
		{
			root->data = x;
		}
		else if (x > root->data && root->right != NULL) 
		{
			insert(x, root->right);
		}
		else if (x < root->data && root->left != NULL) 
		{
			insert(x, root->left);
		}
		else if (x > root->data) 
		{
			Node* p = new Node();
			p->data = x;
			root->right = p;
		}
		else {
			Node* p = new Node();
			p->data = x;
			root->left = p;
		}
		return root;
	}

	void create(int n) 
	{
		int x;
		while (n--) {
			cin >> x;
			insert(x, root);
		}
	}
	void MidOrder(Node* root) //中序遍历
	{
		if (root->left != NULL)
		{
			MidOrder(root->left);
		}
		cout << root->data << " ";
		if (root->right != NULL)
		{
			MidOrder(root->right);
		}
	}
	void Search(int m) 
	{
		int x;
		while (m--) 
		{
			cin >> x;
			int num = search(x, root);
			if (num < 0)
			{
				cout << "-1" << endl;
			}
			else
			{
				cout << num << endl;
			}
		}
	}
	int search(int x, Node* r) //递归调用
	{
		if (r->data > x && r->left == NULL)
		{
			return -9999;
		}
		if (r->data < x && r->right == NULL)
		{
			return -9999;
		}
		if (r->data == x)
		{
			return 1;
		}
		if (r->data > x)//大于x找左数
		{
			return 1 + search(x, r->left);//查找一次就加1
		}
		if (r->data < x)//小于x找右数
		{
			return 1 + search(x, r->right);
		}

	}

};
int main() {
	int t, n, m;
	cin >> t;
	BSTree Mytree;
	while (t--) {
		cin >> n;
		Mytree.create(n);
		Mytree.MidOrder(Mytree.getroot());
		cout << endl;
		cin >> m;
		Mytree.Search(m);
	}
	return 0;
}