﻿// E. DS队列----银行单队列多窗口模拟.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//
#include <iostream>
#include <iomanip>
#include <queue>
#include <stack>
using namespace std;
struct custumer
{
    int T, P;
};
int main()
{
    int N, k;
    cin >> N;
    queue<custumer> que;
    for (int i = 0; i < N; i++)
    {
        custumer temp;

        cin >> temp.T >> temp.P;
        que.push(temp);
    }

    cin >> k;//窗口数
    int sumwait = 0;//总的等待时间
    int maxwait = 0;//最长等待时间
    int  wait = 0;//单次等待时间
    
    int sum[20] = { 0 };//完成时间

    while (!que.empty())
    {
        int flag = 0;//标记变量
        int mint = 99999;//最快完成时间
        int imin = 0;//最快完成时间的下标

        for (int i = 0; i < k; i++)//遍历K个窗口
        {
            if (sum[i] <= que.front().T)//如果队列首位到达的时间比完成时间大，就代表不需要等待
            {
                sum[i]= que.front().T + que.front().P;//更新这个窗口的完成时间
                flag = 1;//表示不需要等待
                que.pop();//去除首位
                break;//退出循环
            }
            if (mint > sum[i])//如果需要等待，就记录各个窗口里最快完成的那个窗口的完成时间和下标
            {
                mint = sum[i];
                imin = i;
            }
        }
        if (flag==0)//需要等待
        {
            //等待的时间 = 最快完成的时间 - 队列第一个人到达的时间
            wait = mint - que.front().T;
            if (maxwait < wait)//不断更新等待的最长时间
            {
                maxwait = wait;
            }
            sumwait += wait;//求等待时间的和
            sum[imin] = mint + que.front().P;//更新对应窗口的完成时间
            que.pop();//队列删除首位
        }
    }
    int last = 0;
    for (int i = 0; i < k; i++)
    {
        if (last < sum[i])
        {
            last = sum[i];//求最大完成时间
        }      
    }
    cout << fixed << setprecision(1)
        << sumwait * 1.0 / N << " " << maxwait << " " << last << endl;
    return 0;
}
