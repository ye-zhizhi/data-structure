﻿// D. DS串应用--串替换.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <string>
using namespace std;
void GetNext(string zi_chuan, int* next, int lenzi)
{
    next[0] = -1;
    next[1] = 0;
    int i = 2;
    int k = 0;
    while (i < lenzi)
    {
        if (k == -1 || zi_chuan[i - 1] == zi_chuan[k])
        {
            next[i] = k + 1;
            k++;
            i++;
        }
        else
        {
            k = next[k];
        }
    }
}
int KMP(string zhu_chuan, string zi_chuan)
{
    int lenzhu = zhu_chuan.length();
    int lenzi = zi_chuan.length();
    if (lenzhu == 0 || lenzi == 0)
    {
        return -1;
    }

    //求next数组
    int* next = new int[lenzi];
    GetNext(zi_chuan, next, lenzi);

    int i = 0;//遍历主串
    int j = 0;//遍历子串
    while (i < lenzhu && j < lenzi)
    {
        if (j==-1||zhu_chuan[i] == zi_chuan[j])
        {
            i++;
            j++;
        }
        else
        {
            j = next[j];
        }
    }
    if (j >= lenzi)
    {
        return i - j;
    }
    return -1;
}

int main()
{
    string zhu_chuan;
    string zi_chuan;
    string ti_huan;
    int t = 0;
    cin >> t;
    while (t--)
    {
        cin >> zhu_chuan >> zi_chuan >> ti_huan;
        int pos = KMP(zhu_chuan, zi_chuan);//返回子串在主串出现第一个字符的位置
        string tmp = zhu_chuan;

        cout << zhu_chuan << endl;    
        if (pos != -1)
        {
            tmp.erase(pos, zi_chuan.length());
            tmp.insert(pos, ti_huan);
        }
        cout << tmp << endl;
    }
}


