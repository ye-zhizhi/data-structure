﻿// DS栈—波兰式，逆波兰式.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include<iostream>
#include<stack>
#include<string.h>
using namespace std;

stack<char> opre;
stack<int> nums;

bool isbigcmp(char a, char b)
{
	if (a == '+' || a == '-')
	{
		if (b == '(' || b == '#' || b == '*' || b == '/')
		{
			return false;
		}
		return true;
	}
	else if (a == '*' || a == '/')
	{
		if (b == '(')
		{
			return false;
		}
		return true;
	}
	else if (a == '(')
	{
		if (b == ')')
		{
			return true;
		}
		return false;
	}
	else if (a == ')')
	{
		return true;
	}
	else if (a == '#')
	{
		if (b == '#')
		{
			return true;
		}
		return false;
	}
}

bool isbigcmp2(char a, char b)
{
	if (a == '+' || a == '-')
	{
		if (b == '(' || b == '#' || b == '*' || b == '/' || b == '+' || b == '-')
		{
			return false;
		}
		return true;
	}
	else if (a == '*' || a == '/')
	{
		if (b == '(')
		{
			return false;
		}
		return true;
	}
	else if (a == '(')
	{
		//if (b == ')')return true;
		return false;
	}
	else if (a == ')')
	{
		return true;
	}
	else if (a == '#')
	{
		//if (b == '#')
		//	return true;
		return false;
	}
}

int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		int temp = 0;
		string fn;
		cin >> fn;
		//前缀表达式：从右到左
		for (int i = fn.length()-1; i >= 0; i--)
		{
			
			if (fn[i] >= '0' && fn[i] <= '9')
			{
				int tot = fn[i] - '0';
				int x = 10;
				while (i - 1 >= 0 && isdigit(fn[i - 1]))
				{
					tot += x * (fn[i - 1] - '0');
					x *= 10;
					i--;
				}
				nums.push(tot);
				temp++;
			}
			else
			{
				if (opre.empty() || opre.top() == ')' || fn[i] == ')')
				{
					opre.push(fn[i]);
				}
				else if (fn[i] == '(')
				{
					while (opre.top() != ')')
					{
						nums.push(opre.top() * 1000000);
						temp++;
						opre.pop();
					}
					//nums.push(opre.top());
					opre.pop();
				}
				else if (isbigcmp(fn[i], opre.top()))
				{
					opre.push(fn[i]);
				}
				else if (!isbigcmp(fn[i], opre.top()))
				{
					while (opre.size() != 0 && !isbigcmp(fn[i], opre.top()))
					{
						nums.push(opre.top() * 1000000);
						temp++;
						opre.pop();
					}
					opre.push(fn[i]);
				}
			}
		}
		while (!opre.empty())
		{
			nums.push(opre.top() * 1000000);
			temp++;
			opre.pop();
		}
		while (!nums.empty())
		{
			int x = nums.top();
			if (x > 1000000)
			{
				x /= 1000000;
				cout << (char)x;
			}
			else
			{
				cout << nums.top();
			}
			nums.pop();
			temp--;
			if (temp > 0)
			{
				cout << " ";
			}
		}
		cout <<"M" << endl;
		//-----------------------------------------------------------------------------
		int temp2 = 0;
		stack
		//后缀表达式：从左到右
		for (int i = 0; i < fn.length(); i++)
		{
			if (fn[i] >= '0' && fn[i] <= '9')
			{
				int tot = fn[i] - '0';
				while (i + 1 < fn.length() && isdigit(fn[i + 1]))
				{
					tot = tot * 10 + (fn[i + 1] - '0');
					i++;
				}
				//nums.push(tot);
				cout << tot << " ";

			}
			else
			{
				if (opre.empty() || opre.top() == '(' || fn[i] == '(')
				{
					opre.push(fn[i]);
				}
				else if (fn[i] == ')')
				{
					while (!opre.empty() && opre.top() != '(')
					{
						//nums.push(opre.top());
						cout << opre.top() << ' ';
						opre.pop();
					}
					//nums.push(opre.top());
					opre.pop();
				}
				else if (isbigcmp2(fn[i], opre.top()))
				{
					opre.push(fn[i]);
				}
				else if (!isbigcmp2(fn[i], opre.top()))
				{
					while (!opre.empty() && !isbigcmp2(fn[i], opre.top()) && opre.top() != '(')
					{
						//nums.push(opre.top());
						cout << opre.top() << ' ';
						opre.pop();
					}
					opre.push(fn[i]);
				}
			}
		}
		while (!opre.empty())
		{
			//nums.push(opre.top());
			cout << opre.top() << " ";
			opre.pop();
		}
		/*while (!nums.empty())
		{
			cout << (char)nums.top()<<" ";
			nums.pop();
		}*/
		if (t > 0)
		{
			cout << "M"<<endl;
			cout << "M"<<endl;
		}

	}
	return 0;
}





