﻿// D. DS顺序表之循环移位.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include<iostream>
using namespace std;

#define ok 0
#define error -1

class SeqList 
{
private:
	int* list;
	int maxsize;
	int size;
public:
	SeqList() 
	{
		maxsize = 1000;
		size = 0;
		list = new int[maxsize];
	}
	~SeqList() 
	{
		delete[]list;
	}
	void push_back(int i) 
	{
		list[size] = i;
		size++;
	}
	void list_display() 
	{
		for (int i = 0; i < size; i++) 
		{
			cout << list[i] << " ";
		}
		cout << endl;
	}
	void move(int des, int num)
	{
		if (des)//向右移动
		{
			for (int j = 0; j < num; j++) 
			{
				//保存边缘数据
				int temp = list[size - 1];
				for (int i = size - 1; i > 0; i--)
				{
					list[i] = list[i - 1];
				}
				list[0] = temp;
			}
		}
		else//向左移动
		{
			for (int j = 0; j < num; j++)
			{
				int temp = list[0];
				for (int i = 0; i < size - 1; i++)
				{
					list[i] = list[i + 1];
				}
				list[size - 1] = temp;
			}
		}
	}
};

int main() {
	SeqList s1;
	int n, a;
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> a;
		s1.push_back(a);
	}
	s1.list_display();
	int des, num;
	cin >> des >> num;
	s1.move(des, num);
	s1.list_display();
	cin >> des >> num;
	s1.move(des, num);
	s1.list_display();

}

