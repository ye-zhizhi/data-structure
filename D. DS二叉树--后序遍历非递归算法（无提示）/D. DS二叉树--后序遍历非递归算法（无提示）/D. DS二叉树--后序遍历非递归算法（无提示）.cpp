﻿// D. DS二叉树--后序遍历非递归算法（无提示）.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <stack>
using namespace std;

class TreeNode {
public:
	char data;
	TreeNode* left;
	TreeNode* right;
	TreeNode() :left(NULL), right(NULL) {}
	~TreeNode() {}
};
class Tree {
private:
	TreeNode* Root;
	int pos;
	string strTree;
	TreeNode* CreateTree();
	void Mirror(TreeNode* t);
public:
	Tree() {};
	~Tree() {};
	void CreateTree(string TreeArray);
	void show();
};
void Tree::CreateTree(string TreeArray) {//建立二叉树 
	pos = 0;
	strTree.assign(TreeArray);
	Root = CreateTree();
}
TreeNode* Tree::CreateTree()
{
	TreeNode* T;
	char ch;
	ch = strTree[pos++];
	if (ch == '0') T = NULL;
	else
	{
		T = new TreeNode();
		T->data = ch;
		T->left = CreateTree();
		T->right = CreateTree();
	}
	return T;
}
void Tree::show()
{
	stack<TreeNode*>node;
	TreeNode* cur = Root;
	TreeNode* leaf = nullptr;
	while (cur || !node.empty())
	{
		while (cur)
		{
			node.push(cur);
			cur = cur->left;
		}
		TreeNode* temp = node.top();
		if (temp->right == nullptr || temp->right == leaf)
		{
			cout << temp->data;
			leaf = temp;
			node.pop();
		}
		else
		{
			cur = temp->right;
		}
	}
}
int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		string str;
		cin >> str;
		Tree t;
		t.CreateTree(str);
		t.show();
		cout << endl;
	}
}


