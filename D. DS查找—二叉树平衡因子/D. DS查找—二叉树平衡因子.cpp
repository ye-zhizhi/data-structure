﻿// D. DS查找—二叉树平衡因子.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//
#include <iostream>
using namespace std;
class Bt {
private:
    char* t;        //存输入的信息
    int n;        //长度
    int* le;      //存左子树深度
    int* ri;     //存右子树深度
public:
    Bt() {};
    ~Bt() {};
    void set_Bt();
    void get_balance_factor(int k);
    void PostOrder(int k);
};
void Bt::set_Bt()
{
    cin >> n;
    t = new char[n];
    for (int i = 0; i < n; i++)
    {
        cin >> t[i];
    }
    le = new int[n];
    ri = new int[n];
    for (int i = 0; i < n; i++)
    {
        le[i] = -1;
        ri[i] = -1;
    }
}
void Bt::get_balance_factor(int k)        //求结点k的左右孩子的深度
{
    int left = k * 2 + 1, right = k * 2 + 2;
    if (left >= n)                        //若左右孩子都不存在，则深度都为0
    {
        le[k] = 0;
        ri[k] = 0;
    }
    else
    {
        if (t[left] == '0')      //若左孩子不存在，则深度为0
            le[k] = 0;
        else
        {
            get_balance_factor(left);     //若左孩子存在，则递归求左孩子的左右子树的深度，然后取大的一个+1作为左子树的深度
            le[k] = le[left] > ri[left] ? le[left] + 1 : ri[left] + 1;
        }

        if (right >= n)
            ri[k] = 0;
        else
        {

            if (t[right] == '0')     //若右孩子不存在，则深度为0
                ri[k] = 0;
            else
            {
                get_balance_factor(right);     //若右孩子存在，则递归求右孩子的左右子树的深度，然后取大的一个+1作为右子树的深度
                ri[k] = le[right] > ri[right] ? le[right] + 1 : ri[right] + 1;
            }
        }

    }
}

void Bt::PostOrder(int k)        //后序输出平衡因子，平衡因子即为左子树深度减右子树深度
{
    if (k >= n || t[k] == '0')
        return;
    int left = k * 2 + 1, right = k * 2 + 2;            //取左右孩子的下标
    PostOrder(left);                        //先输出左孩子的平衡因子
    PostOrder(right);                      //后输出右孩子的平衡因子
    cout << t[k] << ' ' << le[k] - ri[k] << endl;      //最后输出自己的平衡因子
}
int main()
{
    int t;
    cin >> t;
    while (t--)
    {
        Bt temp;
        temp.set_Bt();
        temp.get_balance_factor(0);
        temp.PostOrder(0);
    }
}

