﻿// C. 子串循环问题 (Ver. I).cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

//#include <iostream>
//#include <string>
//using namespace std;
//
//int BF(string str)
//{
//    int len = str.length();//先求字符串长度
//    char str1 = str[0];
//    int x = 0;
//    for (int i = 1; i < len; i++)
//    {
//        if (str1 == str[i])
//        {
//            x = i;
//            break;
//        }
//    }
//    int m = 0;
//    if (x == 0)
//    {
//        return len;
//    }
//    else
//    {
//        if (x == len - 1)
//        {
//            return x - 1;
//        }
//        else
//        {
//            int tmp1 = x;
//            int tmp2 = x - 1;
//
//            while (m <= tmp2&&x<=tmp1+tmp2)
//            {
//                if (str[m] == str[x])
//                {
//                    m++;
//                    x++;
//                }
//            }
//        }
//    }
//}
//int main()
//{
//    int t = 0;
//    string str;
//    cin >> t;
//    while (t--)
//    {
//        cin >> str;
//        cout << BF(str) << endl;
//    }
//}

#include<iostream>
#include<cstring>
using namespace std;

int BF(string str) 
{
    int max = 0, xu;
    string str1, str2;
    for (int j = 0; j < str.length() - 1; j++) 
    {
        str1 = str.substr(0, j + 1);
        str2 = str.substr(str.length() - 1 - j, j + 1);
        if (str1 == str2) 
        {
            max = str1.length();
        }
    }
    int a = str.length() - max;//需要的最小循环子串
    if (str.length() % a == 0&& str.length() != a) 
    {
        cout << '0';
    }
    else 
    {
        cout << a - str.length() % a;
    }
    return 0;
}

int main() 
{
    int t;
    string str;
    cin >> t;
    for (int i = 1; i <= t; i++) 
    {
        cin >> str;
        BF(str);
        if (i < t) 
        {
            cout << endl;
        }
    }
    return 0;
}