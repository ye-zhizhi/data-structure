﻿// DS静态查找之顺序索引查找.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
using namespace std;

void BulidIndex(int k,int n, int* Maintable, int* Indextable, int*&Indexadj)
{
    int i = 0;
    int j = 0;
    for (i = 0; i < k; i++)
    {
        if (i == 0)//第一块的下标一定为0
        {
            Indexadj[i] = 0;
        }
        else
        {
            for (j = 0; j < n; j++)//遍历主表，找出第一个“大于上一块最大关键字 ”的元素
            {
                if (Maintable[j] > Indextable[i - 1])
                {
                    Indexadj[i] = j;
                    break;
                }
            }

        }
    }
}

int main()
{
    int n = 0;
    int k = 0;
    int t = 0;

    cin >> n;
    int* Maintable = new int[n];//主表
    for (int i = 0; i < n; i++)
    {
        cin >> Maintable[i];
    }

    cin >> k;
    int* Indextable = new int[k];//索引表每个块的最大值
    for (int i = 0; i < k; i++)
    {
        cin >> Indextable[i];
    }
    //创建索引表
    int* Indexadj = new int[k];//找出每个块的索引表下标
    BulidIndex(k,n,Maintable, Indextable, Indexadj);

    cin >> t;
    while (t--)
    {
        int key = 0;//要查找的数值
        int cnt = 0;//查找次数
        int i = 0;
        cin >> key;

        //索引表中找块
        int index;
        for (i = 0; i < k; i++)
        {
            cnt++;
            if (key <= Indextable[i])
            {
                index = i;
                break;
            }
        }
        if (i == k)//索引表中都没找到，直接退出
        {
            cout << "error" << endl;
            break;
        }

        //主表对应块中找相应元素
        for (i = Indexadj[index]; i < n; i++)
        {
            cnt++;
            if (Maintable[i] == key)
            {
                cout << i + 1 << "-" << cnt << endl;
                break;
            }
        }
        if (i == n)//找不到
        {
            cout << "error" << endl;
        }
    }
}

