﻿// E. 二叉树的中后序遍历构建及求叶子.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
using namespace std;
class TreeNode {
public:
	int data;
	TreeNode* left;
	TreeNode* right;
	TreeNode()
	{
		left = NULL;
		right = NULL;
	}
};
class Tree
{
private:
	int i = 0;
	TreeNode* root;
    int min = 99999;
	TreeNode* Createtree(int* mid, int* last, int size);
public:
	Tree() {}
	~Tree() {}
	void Create(int*mid,int* last,int size)
	{
		root = Createtree(mid, last, size);
	}
    void Pre(TreeNode* root,int*min);
    int getmin()
    {
        Pre(root, &min);
        return min;
    }
};
TreeNode* Tree::Createtree(int* mid, int* last, int size)
{
    if (size == 0) {
        return NULL;
    }
    TreeNode* T = new TreeNode();
    int i;
    T->data = last[size - 1];

    for (i = 0; mid[i] != last[size - 1]; i++);
    T->left = Createtree(mid, last, i);
    T->right = Createtree(mid + i + 1, last + i, size - i - 1);
    return T;
}
void Tree::Pre(TreeNode* root,int*min)
{
    if (root != NULL)
    {
        if (root->left == NULL && root->right == NULL)
        {
            if (root->data < *min)
                *min = root->data;
        }
        Pre(root->left, min);
        Pre(root->right, min);
    }
}
int main()
{
    int t = 0;
    while (cin >> t)
    {
        int* mid = new int[t];
        for (int i = 0; i < t; i++)
        {
            cin >> mid[i];
        }

        int* last = new int[t];
        for (int i = 0; i < t; i++)
        {
            cin >> last[i];
        }

        Tree mytree;
        mytree.Create(mid, last, t);
        int min = mytree.getmin();
        cout << min << endl;
    }
}

