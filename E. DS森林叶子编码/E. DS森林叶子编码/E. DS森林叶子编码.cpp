﻿#include<iostream>
using namespace std;

int N, B;//N个树，每结点最多B个分支

class treenode//多叉树结点
//多叉树的构造：多叉树的结点结构中，指向孩子的指针应该是二级指针，它需要根据用户的输入动态分配孩子的数目。
// 函数传入根结点，返回根结点，在函数内部使用循环对每个孩子结点进行构造即可。
//因为是动态分配所以不用new两次
{
    char data;
    treenode** child;
public:
    treenode()
    {
        child = new treenode * [B];
        for (int i = 0; i < B; i++)
            child[i] = NULL;
    }
    ~treenode()
    {
        delete[]child;
    }
    friend class forest;
};

class binode//二叉树结点
{
    char data;
    binode* lchild, * rchild;
    binode() :lchild(NULL), rchild(NULL) {}
    friend class forest;
};

class forest//多叉树
{
    treenode** root1;
    binode** root2;

    treenode* create()
    {
        treenode* T;
        int i;
        char ch;

        cin >> ch;
        if (ch == '0')//空树
            T = NULL;
        else
        {
            T = new treenode;
            T->data = ch;
            for (i = 0; i < B; i++)
                T->child[i] = create();
        }
        return T;
    }

    void change(treenode* T, binode*& q, int i)//小多叉树转二叉树
    //多叉树转化成二叉树：这里每个结点的孩子变成二叉树中它的左节点，每个结点的兄弟变成二叉树中它的右节点。
    {
        if (i < B && T->child[i])
        {
            q = new binode;//q是二叉树
            q->data = T->child[i]->data;

            change(T, q->rchild, i + 1);//兄弟(i+1)变成右结点

            change(T->child[i], q->lchild, 0);//孩子变成左结点
            //(q->lchild->data=T->child[i]->child[0]->data)
        }
        else
            q = NULL;
    }

    void postorder(binode* p, string treearray)
    {
        int i;
        if (p)
        {
            postorder(p->lchild, treearray + '0');//左孩子编码为‘0’
            postorder(p->rchild, treearray + '1');//右孩子编码为‘1’

            if (!p->lchild && !p->rchild)//输出编码
            {
                int len = treearray.length();
                for (i = 0; i < len; i++)
                {
                    if (i != len - 1)
                        cout << treearray[i] << " ";
                    else
                        cout << treearray[i] << endl;
                }
            }
        }
    }

public:
    forest()
    {
        root1 = new treenode * [N]; root2 = new binode * [N];
    }
    ~forest()
    {
        delete[]root1; delete[]root2;
    }
    void createbitree()//构造每一颗树
    {
        int i;
        for (i = 0; i < N; i++)
            root1[i] = create();
    }
    void change()//多棵二叉树转大二叉树
    {
        int i;
        for (i = 0; i < N; i++)
        {
            root2[i] = new binode;
            root2[i]->data = root1[i]->data;
            change(root1[i], root2[i]->lchild, 0);//孩子变左结点
        }
        for (i = 0; i < N - 1; i++)
            root2[i]->rchild = root2[i + 1];//兄弟变右结点
    }
    void postorder()
    {
        string treearray;
        postorder(root2[0], treearray);
    }
};

int main()
{
    cin >> N >> B;
    forest F;
    F.createbitree();
    F.change();
    F.postorder();
    return 0;
}
