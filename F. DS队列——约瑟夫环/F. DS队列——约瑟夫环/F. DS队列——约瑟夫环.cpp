﻿// F. DS队列——约瑟夫环.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <queue>
using namespace std;
int main()
{
    int t = 0;
    cin >> t;
    while (t--)
    {
        int n, k;
        cin >> n >> k;
        queue<int> ring;
        for (int i = 1; i <= n; i++)
        {
            ring.push(i);
        }

        while (n)
        {
            for (int i = 1; i < k; i++)
            {
                int tmp = ring.front();
                ring.push(tmp);
                ring.pop();
            }
            cout << ring.front() << " ";
            ring.pop();
            n--;
            if (n == 1)
            {
                cout << ring.front() << endl;
                break;
            }
        }
    }
}
